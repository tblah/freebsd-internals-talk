% Not a conference report: FreeBSD Kernel Internals Course Review
% Tom Eccles

## Introduction
- It is a conference *and training* budget
- I spent most of my 2019 training budget on a series of lectures

## What is FreeBSD?
- Open source operating system derived from 4.4BSD-Lite
- One of the oldest active open source projects
- Popular on servers
  - Root name serviers
  - Verisign
  - Netflix CDN
  - WhatsApp
  - BBC
- BSD license is friendly for non-free appliance products
  - Juniper Networks
  - Apple

# What does the course cover?

## High level view
- (optional) The history of FreeBSD
- 14 ~3 hour lectures covering the algorithms behind FreeBSD's kernel
  - Mostly this is an idealised textbook-like overview, not getting
    too far into the messier hardware details or showing code
  - Most of the content is probably just as applicable to Linux as FreeBSD
  - The instructor tries to highlight where other UNIX-like kernels do
    things differently (e.g. `epoll` vs `kqueue`)
- A brilliant guest lecture about diagnosing performance issues
- A fantastic textbook (available separately)

## Kernel internals lectures
1. Introduction - what does a kernel do?
2. Locking
3. Processes
4. Security (capsicum, MAC framework, jails, disk encryption)
5. Virtual Memory
6. I/O - file descriptors, kqueue, VFS, mounting filesystems
7. Devices - terminals, disks, drivers, virtualisation
8. FFS (UFS)
9. ZFS
10. NFS
11. Networking intro
12. The network layer: IPv4, IPv6, firewalls
13. The transport layer: TCP and SCTP
14. Bootloaders, kernel startup, kernel debugging

## Who teaches the course?
- Marshall Kirk McKusick
- Worked on BSD kernels since the 80s
  - Lots of interesting stories from the development of BSD and TCP/IP
- Designed and wrote a lot of UFS2
- Wrote the textbook
- President of USENIX 1990-1992 2002-2004
- On the editorial board of ACM Queue
- Taught operating systems at Berkeley

## Was it any good?
### Pros
- Excellent instructor
- In-depth
- Covers a lot of ground
- Well worth watching more than once
- Interesting stories about the development of BSD and TCP/IP
- There's an advanced follow up course

### Cons
- $1495.00 (slightly less if you already own the textbook)
- Long
- There is a lot of reading to do (even after skipping some!)

## Any questions?
Find the course at https://www.mckusick.com/courses/introdescrip.html

If anyone is curious, I would recommend buying the textbook first
and seeing if that is interesting.
