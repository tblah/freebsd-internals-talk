PANDOC=pandoc
PDFVIEWER=zathura

# either beamer (pdf); html:  s5, slidy, slideous, dzslides or revealjs
FORMAT=beamer

PANDOC_OPTS=-t $(FORMAT)

INFILE=slides.md
OUTFILE=freebsd_talk.pdf

.PHONY: clean view

view: $(OUTFILE)
	$(PDFVIEWER) $< &

$(OUTFILE): $(INFILE)
	$(PANDOC) $(PANDOC_OPTS) -s $< -o $@

clean:
	rm $(OUTFILE); true
